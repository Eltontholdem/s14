// JS Functions
/*
	Functions are used to create reuseable commands/statements that prevents the dev from typing a bunch of codes. in field, a big number of lines of codes is the normal output, using functions would save the dev a lot of time and effort in typing the codes that will be used multiple times

	SYNTAX
	function functionName(parameters){
		command/statement
	};
*/

function printStar(){
	console.log("*")
};

printStar();
printStar();
printStar();
printStar();

function sayHello(name){
	/*
		functions can also use parameters, these parameters can be defined and a part of the command inside the function. when called, parameters can be replaced with the target value of the developer.  make sure that the value is inside quotations when called
	*/
	console.log("Hello " + name)
};

sayHello("Elton");

/*
function alertPrint(){
	alert("Hello")
	console.log("Hello")
};
alertPrint();

*/

//Function that accepts two numbers and prints the sum
function add(x,y){
	let sum=x+y
	console.log(sum)
};

add(1,2);
add(13,42);
//add(832,449,123); if the number of parameters defined exceeds the needed, the excess would be ignored by JS

//Three parameters
//display the fname, lname, age
function printBio(fname , lname , age ){
	//console.log("Hello " + fname + " "+ lname + " "+age)

	//using template literals
	/*
		declared by the use of backticks with dollar sign and curly braces with the parameters inside the braces. the displayed data in the console will be the defined parameter instead of the text inside the curly braces
	*/
	//backticks - the symbol on the left side of the 1 in the keyboard
	console.log(`Hello ${fname} ${lname} ${age}`)
};

printBio("Elton","Calderon",18);

function createFullName(fname, mname, lname){
	//return specifies the value to be given back by the function once it is finishing executing the value can be given to a variable. It only gives value, but does not display then in the console. that is why we also need to log the variable in the console outside the function
	return `${fname} ${mname} ${lname}`
};

let fullName=createFullName("Elton","Vangie","NoahEeza");
console.log(fullName);




