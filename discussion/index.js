// one line comment (ctrl+slash)
/*
	multiline comment (ctrl+shift+slash)
*/

console.log("Hello Batch 170!");

/*
	Javascript - we can see the messages/log in the console

	Browser Consoles are part of the browsers which will allow us to see/log messages, data or information from the programming language

		they are easily accessed in the dev tools

	Statements
		instructions expressions that we add to the programming lanuage to communicate with the computers

		usually ending with a semicolon (;) However, JS has implemented a way to automatically add semicolons at the end of the line.

	Syntax
		set of rules that describes how statements should be made/ constructed

		lines/ blocks of codes must follow a certain set of rules for it to work properly
*/

	console.log("eltonCalderon");

	/*
	create three console logs to display your favorite food
	*/

	/*console.log("Grilled Beef")
	console.log("Beef Steak")
	console.log("Pepperoni Pizza")*/

	let food1="Grilled Beef"

	console.log(food1)

	/*
		Variables are way to store information or data within the JS

		to create a variable we first declare the name of the variable with either let/const keyword

			let nameOfVariable

		Then, we can initialize(define) the variable with a value or data

			let nameOfVariable=<valueOfVariable>
	*/

	console.log("My favorite food is: "+food1);

	let food2="Beef Steak"
	
	console.log("My favorite food is: "+food1+" and "+food2);


	let food3;

	/* 
		we can create variables without values but the variables content would log undefined
	*/

	console.log(food3);

	food3="Pepperoni Pizza";

	/*
		we can update the content of a variable by reassigning the value using an assignment operator (=);

		assignment operator (=) lets us (re)assign values to a variable
	*/

	console.log(food3);

	const sunriseDirection="East";
	

	/*
	1. We can create a let variable with the let keyword. let variables can be reassigned but not redeclared

	2. Creating a variable has two parts: Declaration of the variable name; and Initialization of the initial value of the variable through the use of assignment operator (=)

	3. A let variable can be declared without initialization. However, the value of the variable will be undefined until it is reasigned with a value

	4. Not defined cs Undefined. Not defined error means that the variable is used but not DECLARED.
	Undefined results from a variable that is used but not INITIALIZED.

	5. We can use const keyword to create variables. Constant variables cannot be declared without initialization. Constant variables values cannot be reassigned

	6. When creating  variable names, start with small caps, this is because we are avoiding conflict with JS that is named with capital letters

	7. If the variable would need two words, the naming convention is the use of camelCase. Do nod add any space for the variables with words as names


	*/
	//Data Types

	/*
	string
	are data types which are alphanumerical text. They could be a name, phrase, or even a sentence.  We can create stringdata types with a single quote(") or with a double quote(") The text inside the quotation marks will be displayed

	keyword variable="string data";
	*/

	console.log("Sample String Data");

	/*
		numeric data types
			are data types which are numeric, numeric data types are displayed when numbers are not included in the quotation marks.  If there is mathematical operation needed to be done, numeric data type should be used instead of string data type.

			keyword variable =  numeric data;
	*/

	console.log(0123456789);
	console.log(1+1);
	console.log("1+1"); /*string data type*/

	//Mini activity
	/*
		using string data type
			display in the console your:
				name (first and last)
				birthday
				province where you live
		using numeric data type
			display in the console your:
				high score in the last game you played
				favorite number
				your favorite number in electric fan/aircon
	*/

	console.log("Ernesto Calderon");
	console.log("January 20");
	console.log("Metro Manila");
	console.log(25646+"points");
	console.log(2);
	console.log(3)